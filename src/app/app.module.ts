import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TestZoneComponent } from './test-zone/test-zone.component';
import { TestChangeDetectorComponent } from './test-change-detector/test-change-detector.component';

@NgModule({
  declarations: [
    AppComponent,
    TestZoneComponent,
    TestChangeDetectorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
