import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

// https://embed.plnkr.co/plunk/d3KGMh
@Component({
  selector: 'app-change-detector',
  templateUrl: './test-change-detector.component.html'
})
export class TestChangeDetectorComponent implements OnInit {
  private onZoneStableSub: Subscription;
  private onZoneUnstableSub: Subscription;

  book = 'Great Expectations';

  constructor(private changeDetection: ChangeDetectorRef) {
    this.changeDetection.detach();
    this.book = 'Old Man & Sea';
  }

  ngOnInit() {
  }

  applyChanges() {
    console.log('applyChanges');
    this.changeDetection.detectChanges();
  }

  // onZoneStable() {
  //   this.logElement.innerHTML = 'Angular zone is stable <br>' + this.logElement.innerHTML;
  // }
  //
  // onZoneUnstable() {
  //   this.logElement.innerHTML = 'Angular zone is unstable <br>' + this.logElement.innerHTML;
  // }
}
