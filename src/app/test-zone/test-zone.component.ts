import {ChangeDetectionStrategy, Component, NgZone, OnInit} from '@angular/core';
import {fromEvent} from 'rxjs';

// https://netbasal.com/optimizing-angular-change-detection-triggered-by-dom-events-d2a3b2e11d87
@Component({
  selector: 'app-test',
  templateUrl: './test-zone.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestZoneComponent implements OnInit {

  value: number;

  constructor(private zone: NgZone) {
    this.value = 0;
  }

  ngOnInit() {
    this.zone.onUnstable.subscribe(() => {
      console.log('onUnstable');
    });

    fromEvent(window, 'resize').subscribe(() => {
      this.value++;
      console.log('resize');
    });

    // this.zone.runOutsideAngular(() => {
    //   fromEvent(window, 'scroll').subscribe(() => {
    //     this.value++;
    //     console.log('scroll');
    //   });
    // });
  }
}
